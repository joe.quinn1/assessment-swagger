package com.jq.assessment;

import static io.restassured.RestAssured.given;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import io.restassured.RestAssured;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@Slf4j
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
class ApplicationTests {
	@LocalServerPort
	private int port;

	@Value("${server.servlet.context-path:/}")
	private String contextPath;

	@BeforeEach
	void setUp() {
		RestAssured.port = port;
		RestAssured.basePath = this.contextPath;
	}

	@Test
	void contextLoads() {
	}
	@Test
	@DisplayName("Generate API Documentation")
	void generateSwaggerDocs() throws IOException {
		var response  = given().when().get("/v3/api-docs.yaml").then().statusCode(200);
		var yaml = response.extract().body().asString().replaceAll(String.valueOf(port), "8080");
		Files.write(Paths.get("open-api.swagger.yaml"), yaml.getBytes(), CREATE, TRUNCATE_EXISTING);
		log.info("swagger/openAPI docs generated -> open-api.swagger.yaml");
	}
}
