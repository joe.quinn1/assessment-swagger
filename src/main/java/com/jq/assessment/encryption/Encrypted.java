package com.jq.assessment.encryption;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Encrypted {
    String cacheKey() default "";
}
