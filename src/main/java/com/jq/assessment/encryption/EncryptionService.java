package com.jq.assessment.encryption;

import static org.apache.commons.lang3.StringUtils.isEmpty;

import com.jq.assessment.model.Assessment;
import java.lang.reflect.Field;
import java.util.function.UnaryOperator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Inspired by https://gitlab.com/dwp/get-your-state-pension/claimservice/-/blob/main/src/main
 * /java/uk/gov/dwp/gysp/claim/EncryptionEventListener.java
 */
@Slf4j
@Component
public class EncryptionService {
    public Assessment encrypt(Assessment assessment) {
        iterateFields(assessment, this::encrypt);
        return assessment; // Could also switch to declarative rather than reflection.
    }

    public Assessment decrypt(Assessment assessment) {
        iterateFields(assessment, this::decrypt);
        return assessment;
    }

    private void convertField(Field field, Object source, UnaryOperator<String> conversion)
            throws IllegalAccessException {
        var value = (String) field.get(source);
        if (!isEmpty(value)) { // This means empty strings are not encrypted/decrypted.
            field.set(source, conversion.apply(value));
        }
    }

    private void iterateFields(Object source, UnaryOperator<String> conversion) {
        for (Field field : source.getClass().getDeclaredFields()) {
            try {
                if (field.isAnnotationPresent(Encrypted.class)) {
                    field.setAccessible(true);
                    if (field.get(source) != null) {
                        if (!field.getType().isAssignableFrom(String.class)) {
                            iterateFields(field.get(source), conversion);
                        } else {
                            convertField(field, source, conversion);
                        }
                    }
                    field.setAccessible(false);
                }
            } catch (IllegalAccessException e) {
                throw new EncryptionException(String.format("Unable to access [%s][%s]",
                        source.getClass().getName(), field.getName()), e);
            }
        }
    }

    /**
     * TODO replace with KmsEncryptor
     */
    private String decrypt(String value) {
        var converted = value + "*";
        log.warn("Applying dummy decryption [{}]->[{}]", value, converted);
        return converted;
    }

    /**
     * TODO replace with KmsEncryptor
     */
    private String encrypt(String value) {
        var converted = value + "!";
        log.warn("Applying dummy encryption [{}]->[{}]", value, converted);
        return converted;
    }
}
