package com.jq.assessment.encryption;

import com.jq.assessment.model.Assessment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.mapping.event.AfterConvertCallback;
import org.springframework.data.mongodb.core.mapping.event.AfterSaveCallback;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertCallback;

@Configuration
public class EncryptionConfig {
    @Autowired
    EncryptionService encryptionService;

    /**
     * Invoked before a domain object is saved.
     * Can modify the domain object, to be returned after save, Document containing all mapped
     * entity information. Called in MongoTemplate insert, insertList, and save operations after
     * inserting or saving the Document in the database.
     *
     * @return decrypted object.
     */
    @Bean
    AfterSaveCallback<Assessment> afterSave() {
        return (entity, document, collection) -> encryptionService.decrypt(entity);
    }

    /**
     * Invoked after a domain object is loaded.
     * Can modify the domain object after reading it from an org.bson.Document. Called in
     * MongoTemplate find, findAndRemove, findOne, and getCollection methods after the Document
     * has been retrieved from the database was converted to a POJO.
     * @return decrypted object.
     */
    @Bean
    AfterConvertCallback<Assessment> afterConvert() {
        return (entity, document, collection) -> encryptionService.decrypt(entity);
    }

    /**
     * Invoked before a domain object is converted to org.bson.Document. Called in MongoTemplate
     * insert, insertList, and save operations before the object is converted to a Document by a
     * MongoConverter.
     * @return encrypted object.
     */
    @Bean
    BeforeConvertCallback<Assessment> beforeConvert() {
        return (entity, collection) -> encryptionService.encrypt(entity);
    }
}
