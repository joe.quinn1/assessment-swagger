package com.jq.assessment.api.dto;

public record CreateNote(String text) {
}
