package com.jq.assessment.api.dto;

import java.util.List;

public record CreateFailedSecurityEvent(String nino,
                                        List<String> failedQuestions) {
}
