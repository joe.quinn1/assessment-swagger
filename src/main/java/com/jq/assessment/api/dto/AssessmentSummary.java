package com.jq.assessment.api.dto;

import com.jq.assessment.model.Assessment;

public record AssessmentSummary(
        String id,
        String claimId,
        Assessment.Decision decision) {
    public static AssessmentSummary from(Assessment assessment) {
        return new AssessmentSummary(
                assessment.getId(),
                assessment.getClaimId(),
                assessment.getDecision());
    }
}
