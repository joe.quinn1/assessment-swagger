package com.jq.assessment.api;

import static com.jq.assessment.model.Assessment.SubtaskStatus.DECISION_CONFIRMED;
import static com.jq.assessment.model.Assessment.SubtaskStatus.DECISION_NOT_CONFIRMED;
import static com.jq.assessment.model.Assessment.SubtaskStatus.PENDING;
import static com.jq.assessment.model.Subtask.Type.NAME_MATCHES;
import static com.jq.assessment.model.Subtask.Type.TELEPHONE_MATCHES;
import static java.lang.Boolean.TRUE;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import com.fasterxml.jackson.annotation.JsonView;
import com.jq.assessment.api.dto.AssessmentSummary;
import com.jq.assessment.api.dto.CreateNote;
import com.jq.assessment.db.AssessmentRepository;
import com.jq.assessment.model.Assessment;
import com.jq.assessment.model.Note;
import com.jq.assessment.model.Subtask;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.api.annotations.ParameterObject;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@AllArgsConstructor
@RequestMapping("/v1/assessments")
@RestController
@OpenAPIDefinition(info = @Info(
        title = "Pension Credit Assessment Service",
        description = "REST API for Pension Credit Assessments"))
@Tag(name = "Assessments V1")
public class AssessmentController {
    /** HTTP header: total results in database. */
    private static final String TOTAL_RESULTS = "X-Total";

    /** HTTP header: total pages returned. */
    private static final String TOTAL_PAGES = "X-Total-Pages";

    /** HTTP header: current page number. */
    private static final String CURRENT_PAGE = "X-Page";

    /** HTTP header: results per page. */
    private static final String RESULTS_PER_PAGE = "X-Per-Page";

    private AssessmentRepository assessmentRepository;

    @Operation(
            summary = "Get assessment by ID",
            description = "Returns an assessment object by its primary key",
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(content = @Content(schema = @Schema()), responseCode = "404", description = "Assessment not found")
            })
    @GetMapping("/{id}")
    public ResponseEntity<Assessment> getAssessment(@RequestHeader String userid,
                                                    @PathVariable String id) {
        return assessmentRepository.findById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @Operation(
            summary = "Work on assessment",
            description = "Returns an assessment object by its primary key",
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(content = @Content(schema = @Schema()), responseCode = "404", description = "Assessment not found")
            })
    @PutMapping("/{id}/workOn")
    public ResponseEntity<Assessment> workOnAssessment(@RequestHeader String userid,
                                                    @PathVariable String id) {
        return assessmentRepository.findById(id)
                // set assignedTime if not already set + Lock previous rows? + userid?
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @Operation(
            summary = "Confirm assessment by ID",
            description = "Confirm the outcome of an assessment.",
            responses = {
                    @ApiResponse(responseCode = "202", description = "Confirmed and notification issued", content = @Content(schema = @Schema())),
                    @ApiResponse(responseCode = "204", description = "Confirmed and rejected (sent to BAU)", content = @Content(schema = @Schema())),
                    @ApiResponse(responseCode = "400", description = """
                            Validation failure - assessment has a subTaskStatus of PENDING
                            state and so can't be confirmed
                            """,
                            content = @Content(schema = @Schema()))
            })
    @PutMapping("/{id}/confirm")
    public ResponseEntity<Void> confirm(@RequestHeader String userid,
                                                    @PathVariable String id) {
        return assessmentRepository.findById(id)
//                .map()
                .map(this::mapAdvisorCheckStatus)
                .orElse(ResponseEntity.notFound().build());
    }
    private ResponseEntity<Void> mapAdvisorCheckStatus(Assessment assessment) {
        return switch(assessment.getSubtaskStatus()) {
            case DECISION_CONFIRMED -> ResponseEntity.accepted().build();
            case DECISION_NOT_CONFIRMED -> ResponseEntity.noContent().build();
            default -> ResponseEntity.badRequest().build();
        };
    }
    @Operation(
            summary = "Get details of assessment subtask",
            description = "Show details of an individual sub task of an assessment",
            responses = {
                    @ApiResponse(responseCode = "200", description = "OK"),
                    @ApiResponse(responseCode = "404", description = "Subtask not found",
                            content = @Content(schema = @Schema()))
            })
    @GetMapping("/{id}/subtasks/{type}")
    public ResponseEntity<Subtask> getSubtask(@RequestHeader String userid,
                                              @PathVariable String id,
                                              @PathVariable Subtask.Type type) {
        return assessmentRepository.findById(id)
                .map(Assessment::getSubtasks)
                .map(advisorChecks -> advisorChecks.get(type))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @Operation(
            summary = "Update assessment subtask",
            description = "Update an individual sub task of an assessment",
            responses = {
                    @ApiResponse(responseCode = "204", description = "Subtask updated",
                            content = @Content(schema = @Schema())),
                    @ApiResponse(responseCode = "404", description = "Subtask not found",
                            content = @Content(schema = @Schema()))
//                    @ApiResponse(responseCode = "409", description = "Subtask is locked for editing")
            })
    @PutMapping("/{id}/subtasks/{type}")
    public ResponseEntity<Void> updateSubtask(@RequestHeader String userid,
                                              @PathVariable String assessmentId,
                                              @PathVariable Subtask.Type type,
                                              @RequestBody Subtask subTask) {
        return assessmentRepository.findById(assessmentId)
                .map(assessment -> {
                    assessment.getSubtasks().put(type, subTask);
                    return assessment;
                })
                .map(this::assessSubtaskStatus)
                .map(assessment -> assessmentRepository.save(assessment))
                .map(assessment -> assessment.getSubtasks().get(type))
                .map(assessment -> ResponseEntity.noContent().<Void>build())
                .orElse(ResponseEntity.notFound().build());
    }

    private Assessment assessSubtaskStatus(Assessment assessment) {
        var subtasks = assessment.getSubtasks();

        if (subtasks.getOrDefault(NAME_MATCHES, Subtask.empty()).getOutcome() != null
            && subtasks.getOrDefault(TELEPHONE_MATCHES, Subtask.empty()).getOutcome() != null) {

            if (TRUE.equals(subtasks.getOrDefault(NAME_MATCHES, Subtask.empty()).getOutcome())
                    && subtasks.getOrDefault(TELEPHONE_MATCHES, Subtask.empty()).getOutcome()) {
                return assessment.withSubtaskStatus(DECISION_CONFIRMED);
            } else {
                return assessment.withSubtaskStatus(DECISION_NOT_CONFIRMED);
            }
        } else {
            return assessment.withSubtaskStatus(PENDING);
        }
    }


    /** List Assessments with paging support. */
    @Operation(
            summary = "List existing assessments",
            description =
                    "Supports full paging and sorting of results. "
                            + "By default 20 results are returned at a time, and results are sorted by id (asc).",
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Successful",
                            headers = {
                                    @Header(name = TOTAL_RESULTS, description = "Total number of results"),
                                    @Header(name = TOTAL_PAGES, description = "Total pages of results"),
                                    @Header(name = CURRENT_PAGE, description = "Current page number"),
                                    @Header(name = RESULTS_PER_PAGE, description = "Number of results per page")
                            })
            })
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AssessmentSummary>> list(
            @Parameter(
                    description =
                            "optionally filter by nino using hashed value "
                                    + "(be sure to hash using the same salt as used when the claim was created)")
            @RequestHeader(required = false) Optional<String> nino,
            @RequestParam(required = false) List<Assessment.Decision> decisions,
            @RequestParam(required = false, defaultValue = "0") int page,
            @RequestParam(required = false, defaultValue = "100") @Max(1000) int size,
            @RequestParam(required = false, defaultValue = "ASC") Sort.Direction dir,
            @RequestParam(required = false, defaultValue = "id") String... sort) {

        var assessmentsPage =  assessmentRepository.findAll(PageRequest.of(page, size, Sort.by(dir, sort)));

        nino.ifPresent(n -> log.info("nino present - TODO should filter results here!!"));
        log.info("Also need to filter by [{}]", decisions);
        var headers = new HttpHeaders();
        headers.add(TOTAL_RESULTS, String.valueOf(assessmentsPage.getTotalElements()));
        headers.add(TOTAL_PAGES, String.valueOf(assessmentsPage.getTotalPages()));
        headers.add(CURRENT_PAGE, String.valueOf(page));
        headers.add(RESULTS_PER_PAGE, String.valueOf(size));
        var assessmentSummaries = assessmentsPage
                .get()
                .map(AssessmentSummary::from)
                .toList();
        return new ResponseEntity<>(assessmentSummaries, headers, OK);
    }
    @Operation(
            summary = "List notes for an assessment",
            description =
                    "List notes for an assessment",
            responses = {@ApiResponse(responseCode = "200", description = "Successful"),
                    @ApiResponse(
                            responseCode = "404",
                            content = @Content(schema = @Schema()),
                            description = "Assessment not found."),})
    @GetMapping(value="/{id}/notes/", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Note>> listNotes(@PathVariable String id) {
        return assessmentRepository.findById(id)
                .map(Assessment::getNotes)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @Operation(
            summary = "Create note",
            description = "Submit a new note for an assessment",
            responses = {
                    @ApiResponse(
                            content =
                            @Content(
                                    schema = @Schema(implementation = Note.class),
                                    mediaType = MediaType.APPLICATION_JSON_VALUE),
                            responseCode = "201",
                            description = "Note created"),
                    @ApiResponse(
                            responseCode = "404",
                            content = @Content(schema = @Schema()),
                            description = "Assessment not found."),
                    @ApiResponse(
                            responseCode = "400",
                            content = @Content(schema = @Schema()),
                            description = "Supplied note validation failed.")
            })
    @PostMapping(value = "/{id}/notes/", produces = MediaType.APPLICATION_JSON_VALUE)
    public Note create(@RequestBody CreateNote toCreate) {
        return Note.from(toCreate);
    }
    // FIXME: Convenience methods for testing...
    @Hidden
    @PostMapping("/create/{nino}")
    public ResponseEntity<Assessment> createForNino(@PathVariable String nino) {
        log.info("Creating for nino {}", nino);
        var assessment = assessmentRepository.save(Assessment.create(nino));
        return ResponseEntity
                .created(URI.create("/v1/assessments/" + assessment.getId()))
                .body(assessment);
    }

    @Hidden
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable String id) {
        assessmentRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
