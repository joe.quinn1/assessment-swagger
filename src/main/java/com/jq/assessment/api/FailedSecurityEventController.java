package com.jq.assessment.api;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import com.jq.assessment.api.dto.CreateFailedSecurityEvent;
import com.jq.assessment.model.FailedSecurityEvent;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/failedSecurityEvents")
@Tag(name = "Failed Security Events V1")
public class FailedSecurityEventController {

    @Operation(
            summary = "List existing failed security events for a nino",
            description =
                    "Nino needs to be supplied as a header variable",
            responses = {@ApiResponse(responseCode = "200", description = "Successful")})
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public List<FailedSecurityEvent> list(@RequestHeader String nino) {
        return List.of(new FailedSecurityEvent(
                "assddffgg",
                "9dwiuh7hifuwe",
                LocalDateTime.now(),
                LocalDateTime.now().plusMonths(14),
                List.of("When did you last shave?", "What's your favourite colour?")));
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(
            summary = "Create failed security event",
            description = "Submit a new failed security event",
            responses = {
                    @ApiResponse(
                            content =
                            @Content(
                                    schema = @Schema(implementation = FailedSecurityEvent.class),
                                    mediaType = MediaType.APPLICATION_JSON_VALUE),
                            responseCode = "201",
                            description = "Failed security event created"),
                    @ApiResponse(
                            responseCode = "404",
                            content = @Content(schema = @Schema()),
                            description = "Failed security event not valid."),
                    @ApiResponse(
                            responseCode = "400",
                            content = @Content(schema = @Schema()),
                            description = "Supplied failed security event failed validation.")
            })
    public FailedSecurityEvent create(@RequestBody CreateFailedSecurityEvent toCreate) {
        return FailedSecurityEvent.from(toCreate);
    }
}
