package com.jq.assessment.model;

public record RuleResult(String ruleName, boolean matched) {
}
