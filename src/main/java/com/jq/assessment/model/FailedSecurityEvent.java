package com.jq.assessment.model;

import com.jq.assessment.api.dto.CreateFailedSecurityEvent;
import java.time.LocalDateTime;
import java.util.List;

public record FailedSecurityEvent(String id,
                                  String ninoHash,
                                  LocalDateTime createdTime,
                                  LocalDateTime expiryTime,
                                  List<String> failedQuestions
) {
    public static FailedSecurityEvent from(CreateFailedSecurityEvent toCreate) {
        return new FailedSecurityEvent(
                null,
                toCreate.nino(),
                LocalDateTime.now(),
                LocalDateTime.now().plusMonths(14),
                toCreate.failedQuestions());
    }
}

