package com.jq.assessment.model;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Subtask{
    public enum Type {
        NAME_MATCHES,
        TELEPHONE_MATCHES,
        ADDRESS_MATCHES,
        BANK_NAME_MATCHES,
        TELEPHONE_APPLICATION_PRESENT,
        INITIAL_DATE_OF_CONTACT_PRESENT,
        ONLINE_APPLICATION_PRESENT,
        PC1_APPLICATION_PRESENT,
        NO_DATE_DEATH,
        OPEN_INTERESTS_FOR_QUALIFYING_BENEFITS_PRESENT,
        MOST_RECENT_STATE_PENSION_AWARD_VERIFIED,
        BACKDATED_STATE_PENSION_AWARD_VERIFIED,
        QUALIFYING_BENEFITS_PRESENT,
    }
    Boolean outcome;
    String completedByUserId;

    public static Subtask empty() {
        return new Subtask( null, null);
    }
}
