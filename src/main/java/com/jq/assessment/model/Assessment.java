package com.jq.assessment.model;

import static com.jq.assessment.model.Subtask.Type.ADDRESS_MATCHES;
import static com.jq.assessment.model.Subtask.Type.BACKDATED_STATE_PENSION_AWARD_VERIFIED;
import static com.jq.assessment.model.Subtask.Type.BANK_NAME_MATCHES;
import static com.jq.assessment.model.Subtask.Type.INITIAL_DATE_OF_CONTACT_PRESENT;
import static com.jq.assessment.model.Subtask.Type.MOST_RECENT_STATE_PENSION_AWARD_VERIFIED;
import static com.jq.assessment.model.Subtask.Type.NAME_MATCHES;
import static com.jq.assessment.model.Subtask.Type.NO_DATE_DEATH;
import static com.jq.assessment.model.Subtask.Type.ONLINE_APPLICATION_PRESENT;
import static com.jq.assessment.model.Subtask.Type.OPEN_INTERESTS_FOR_QUALIFYING_BENEFITS_PRESENT;
import static com.jq.assessment.model.Subtask.Type.PC1_APPLICATION_PRESENT;
import static com.jq.assessment.model.Subtask.Type.QUALIFYING_BENEFITS_PRESENT;
import static com.jq.assessment.model.Subtask.Type.TELEPHONE_APPLICATION_PRESENT;
import static com.jq.assessment.model.Subtask.Type.TELEPHONE_MATCHES;
import static java.time.LocalDateTime.now;

import com.fasterxml.jackson.annotation.JsonValue;
import com.jq.assessment.encryption.Encrypted;
import io.swagger.v3.oas.annotations.media.Schema;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Value;
import lombok.With;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@AllArgsConstructor
@Builder @Value
public class Assessment {
    @Id
    String id;
    @Pattern(regexp = NINO_REGEX)
    @Schema(description = "National Insurance Number", example = "MW000437A", required = true)
    @With
    String nino;
    String guid;
    @Encrypted
    String claimId;
    @Encrypted
    Facts facts;
    Map<Subtask.Type, Subtask> subtasks;
    @With
    SubtaskStatus subtaskStatus;
    RuleBookName matchingRuleBook;
    List<RuleBookResult> ruleBookResults;
    // RulesAssessed - matching and non-matching?
    Decision decision;
    String confirmedByUserId;
    LocalDateTime confirmedByTime;
    LocalDateTime assignedTime;
    String assignedUser;
    List<Note> notes;

    LocalDateTime expiryTime;
    LocalDateTime created;
    Integer version;

    @AllArgsConstructor
    public enum RuleBookName {
        /**
         * Nil Award Scenario 1.
         */
        @Schema(description = "Nil Award Scenario 1")
        NIL_AWARD_SCENARIO_1("nil.award.1"),

        /**
         * PC In Payment.
         */
        @Schema(description = "PC in Payment")
        ALREADY_RECEIVED_PENSION_CREDIT("already.received.pension.credit");

        /**
         * Forces Json marshalling to use the identifier value rather than the enum name.
         */
        @JsonValue
        @Getter
        final String identifier;

        /**
         * This is needed to get swagger definition to refer to correct values.
         *
         * @return string representing enum.
         */
        @Override
        public String toString() {
            return identifier;
        }
    }

    public enum SubtaskStatus {
        PENDING,
        DECISION_CONFIRMED,
        DECISION_NOT_CONFIRMED
    }

    public enum Decision {
        PENDING,
        NOT_ENTITLED,
        PROCESS_IN_LEGACY,
        PROCESS_IN_LEGACY_FAILED_CHECK
    }

    public static final String NINO_REGEX = "[A-Z]{2}[0-9]{6}[A-Z]";

    public static Assessment create(String nino) {
        Map<Subtask.Type, Subtask> subtasks = new HashMap();
        subtasks.put(NAME_MATCHES, Subtask.empty());
        subtasks.put(TELEPHONE_MATCHES, Subtask.empty());
        subtasks.put(ADDRESS_MATCHES, Subtask.empty());
        subtasks.put(BANK_NAME_MATCHES, Subtask.empty());
        subtasks.put(TELEPHONE_APPLICATION_PRESENT, Subtask.empty());
        subtasks.put(INITIAL_DATE_OF_CONTACT_PRESENT, Subtask.empty());
        subtasks.put(ONLINE_APPLICATION_PRESENT, Subtask.empty());
        subtasks.put(PC1_APPLICATION_PRESENT, Subtask.empty());
        subtasks.put(NO_DATE_DEATH, Subtask.empty());
        subtasks.put(OPEN_INTERESTS_FOR_QUALIFYING_BENEFITS_PRESENT, Subtask.empty());
        subtasks.put(MOST_RECENT_STATE_PENSION_AWARD_VERIFIED, Subtask.empty());
        subtasks.put(BACKDATED_STATE_PENSION_AWARD_VERIFIED, Subtask.empty());
        subtasks.put(QUALIFYING_BENEFITS_PRESENT, Subtask.empty());

        return Assessment.builder()
                .nino(nino)
                .claimId("test-claim-id")
                .guid("GGGUUIIIDD")
                .facts(Facts.builder()
                        .dateOfBirth(LocalDate.now())
                        .firstName("Snake")
                        .lastName("Plissken").build())
                .subtasks(subtasks)
                .assignedTime(LocalDateTime.now())
                .subtaskStatus(SubtaskStatus.DECISION_NOT_CONFIRMED)
                .decision(Decision.PROCESS_IN_LEGACY_FAILED_CHECK)
                .ruleBookResults(List.of(new RuleBookResult("nil.entitled.cohort1",
                        List.of(new RuleResult("spa.amount.greater.than.threshold", true),
                                new RuleResult("no.qualifyingBenefits", false)
                                ))))
                .created(now())
                .build();
    }
}
