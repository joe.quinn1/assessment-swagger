package com.jq.assessment.model;

import com.jq.assessment.api.dto.CreateNote;
import java.time.LocalDateTime;

public record Note(LocalDateTime createdTime,
                   String text
                   ) {
    public static Note from(CreateNote toCreate) {
        return new Note(
                LocalDateTime.now(),
                toCreate.text());
    }
}
