package com.jq.assessment.model;

import com.jq.assessment.encryption.Encrypted;
import io.swagger.v3.oas.annotations.media.Schema;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Value;
import lombok.experimental.Accessors;


/** Facts: Derived facts about the claim used as a basis to perform rules checks against. */
@Builder
@Value
public class Facts {
  public static final Benefit[] OTHER_BENEFITS_TO_CHECK =
      new Benefit[] {
        Benefit.ATTENDANCE_ALLOWANCE,
        Benefit.CARERS_ALLOWANCE,
        Benefit.CONSTANT_ATTENDANCE_ALLOWANCE,
        Benefit.DISABILITY_LIVING_ALLOWANCE,
        Benefit.PENSION_CREDIT,
        Benefit.PERSONAL_INDEPENDENCE_PAYMENT,
        Benefit.ADULT_DISABILITY_PAYMENT
      };

  public static final Benefit[] OTHER_INTERESTS_TO_CHECK =
      new Benefit[] {
        Benefit.ATTENDANCE_ALLOWANCE,
        Benefit.DISABILITY_LIVING_ALLOWANCE,
        Benefit.PENSION_CREDIT,
        Benefit.PERSONAL_INDEPENDENCE_PAYMENT,
        Benefit.CONSTANT_ATTENDANCE_ALLOWANCE,
        Benefit.ADULT_DISABILITY_PAYMENT,
        Benefit.WAR_PARENTS_PENSION,
        Benefit.WAR_ORPHANS_PENSION,
        Benefit.WAR_DISABLEMENT_PENSION,
        Benefit.WAR_PENSION_UNEMPLOYABILITY_SUPPLEMENT,
        Benefit.WAR_PENSION_UNEMPLOYABILITY_SUPPLEMENT_ALT,
        Benefit.WAR_PENSION_MOBILITY_SUPPLEMENT,
        Benefit.WAR_WIDOWS,
        Benefit.WAR_WIDOWERS_PENSION
      };

  public static final Benefit[] OTHER_INTERESTS_NON_CIS_BENEFITS =
      new Benefit[] {
        Benefit.WAR_PARENTS_PENSION,
        Benefit.WAR_ORPHANS_PENSION,
        Benefit.WAR_DISABLEMENT_PENSION,
        Benefit.WAR_PENSION_UNEMPLOYABILITY_SUPPLEMENT,
        Benefit.WAR_PENSION_UNEMPLOYABILITY_SUPPLEMENT_ALT,
        Benefit.WAR_PENSION_MOBILITY_SUPPLEMENT,
        Benefit.WAR_WIDOWS,
        Benefit.WAR_WIDOWERS_PENSION
      };

  /** Benefits we are interested in and their corresponding 'serviceKey' CIS identifiers. */
  @AllArgsConstructor
  public enum Benefit {
    @Schema(description = "Attendance Allowance")
    ATTENDANCE_ALLOWANCE(1),
    @Schema(description = "Carer's Allowance")
    CARERS_ALLOWANCE(6),
    @Schema(description = "State Pension")
    STATE_PENSION(8),
    @Schema(description = "Constant Attendance Allowance.")
    CONSTANT_ATTENDANCE_ALLOWANCE(16),
    @Schema(description = "War Disablement Pension")
    WAR_DISABLEMENT_PENSION(22),
    @Schema(description = "War Widows")
    WAR_WIDOWS(23),
    @Schema(description = "War Orphan's Pension")
    WAR_ORPHANS_PENSION(24),
    @Schema(description = "War Widowers Pension")
    WAR_WIDOWERS_PENSION(25),
    @Schema(description = "War Parent's Pension")
    WAR_PARENTS_PENSION(26),
    @Schema(description = "Adult Dependant's Pension")
    ADULT_DEPENDANTS_PENSION(27),
    @Schema(description = "Disability Living Allowance.")
    DISABILITY_LIVING_ALLOWANCE(31),
    @Schema(description = "Pension Credit.")
    PENSION_CREDIT(80),
    @Schema(description = "War Pension Unemployability Supplement")
    WAR_PENSION_UNEMPLOYABILITY_SUPPLEMENT(131),
    @Schema(description = "War Pension Mobility Supplement")
    WAR_PENSION_MOBILITY_SUPPLEMENT(138),
    @Schema(description = "War Pension Unemployability Supplement Alt")
    WAR_PENSION_UNEMPLOYABILITY_SUPPLEMENT_ALT(326),
    @Schema(description = "Personal Independence Payment.")
    PERSONAL_INDEPENDENCE_PAYMENT(675),
    @Schema(description = "Adult Disability Payment.")
    ADULT_DISABILITY_PAYMENT(770);

    @Getter int dwpServiceKey;

    public static Optional<Benefit> valueOf(int dwpServiceKey) {
      return Arrays.stream(values()).filter(b -> b.dwpServiceKey == dwpServiceKey).findFirst();
    }
  }

  /** Sex. */
  public enum Sex {
    @Schema(description = "Male")
    M,

    @Schema(description = "Female")
    F,
  }

  /** Status of matching person against DWP records. */
  public enum MatchStatus {
    /** Call to match person is still pending (default). */
    @Schema(description = "Call to match person is still pending (default status).")
    PENDING,
    /** Call to match API failed. */
    @Schema(description = "Call to match API failed.")
    CALL_FAILED,
    /** Date of Birth and Nino match an existing record. */
    @Schema(description = "Date of Birth and Nino match an existing record.")
    DOB_AND_NINO,
    /** Does match an existing record. */
    @Schema(description = "Does match an existing record.")
    NO_MATCH
  }
  @Schema(example = "Snake")
  @Encrypted
  String firstName;
  @Schema(example = "Plissken")
  String lastName;
  /** Claimant's sex. */
  Sex sex;

  /** Claimant's date of birth. */
  LocalDate dateOfBirth;

  /**
   * PC applicant is applying for themselves or receiving help for a friend, family member, charity
   * or organisation.
   */
  boolean applyingForThemselvesOrGettingHelp;

  /** Claimant has a date of death. */
  @Accessors(fluent = true)
  boolean hasDeathDate;

  /** State pension weekly amount. */
  @Schema(
      description = "State pension weekly amount.",
      accessMode = Schema.AccessMode.READ_ONLY,
      example = "275.00")
  BigDecimal statePensionWeeklyAmount;

  /** Guaranteed Credit Single Threshold. **/
  @Schema(description = "Guaranteed Credit minimum amount threshold for current single awards")
  BigDecimal guaranteedCreditSingleThreshold;

  /** Pension credit award amount. */
  @Schema(
      description = "Pension Credit award amount.",
      accessMode = Schema.AccessMode.READ_ONLY,
      example = "275.00")
  BigDecimal pensionCreditAwardAmount;

  /** Open Interests. */
  List<Benefit> openInterests;

  /** Benefits in payment. */
  List<Benefit> benefitsInPayment;

  /** Responsible for service charges?. */
  boolean responsibleForServiceCharges;

  /** Responsible for ground rent?. */
  boolean responsibleForGroundRent;

  /** has Partner. */
  boolean hasPartner;

  /** Lives with partner. */
  boolean liveWithPartner;

  /** Match Status. */
  MatchStatus matchStatus;

  /** pending benefits. */
  @Accessors(fluent = true)
  boolean pendingBenefits;

  /** Wants to apply for housing Benefit. */
  @Accessors(fluent = true)
  boolean wantsToApplyForHousingBenefit;

  /** Reapply for support for Mortgage Interest. */
  @Accessors(fluent = true)
  boolean wantsFormsToReapplySupportForMortgageInterest;

  /** Support with interest. */
  @Accessors(fluent = true)
  boolean wantsInfoAboutSupportForMortgageWithInterest;

  /** Claimant's date of claim. */
  LocalDate dateOfClaim;

  /** Benefit(s) returned have an 'In Payment' date within the backdating period. */
  @Accessors(fluent = true)
  boolean hasBenefitsInPaymentInBackdatingPeriod;

  /** Interest(s) returned within the backdating period. */
  @Accessors(fluent = true)
  boolean hasInterestInBackdatingPeriod;

  /**
   * State Pension Benefit(s) returned have an 'In Payment' date within the backdating period and
   * are below the Guaranteed Credit threshold.
   */
  @Accessors(fluent = true)
  boolean hasStatePensionBelowThresholdInBackdatingPeriod;

  /** PC Applicant wish to include children in their claim. */
  boolean responsibleForChildrenWhoLiveWithYou;

  /** PC Applicant require communications in alternate format. */
  boolean contactDifferentFormatRequired;

  /** PC applicant does not live in northern ireland. */
  boolean livesInNorthernIreland;

  /** PC applicant has entered a date of claim. */
  @Accessors(fluent = true)
  boolean hasEnteredDateOfClaim;
}
