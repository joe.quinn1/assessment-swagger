package com.jq.assessment.model;

import java.util.List;

public record RuleBookResult(String rulebookName, List<RuleResult> ruleResults) {
}


