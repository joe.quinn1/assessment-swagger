# Assessment Service open API & demo

To run application

1) Start mongo using `docker-compose up`
2) Start application using `mvn spring boot:run`
3) Navigate gito [OpenAPI Spec](http://localhost:8080/swagger-ui.html)

There are also 3 hidden endpoints for testing:
1) create a dummy assessment for a nino via `POST v1/assessments/create/{nino}`
2) list assessments via `GET v1/assessments/`
3) delete an assessment via `DELETE v1/assessments/{id}`





## Plant UML

```plantuml

skinparam backgroundColor white
skinparam defaultFontName Droid Sans
skinparam BoxPadding 10
skinParam ParticipantPadding 20
actor "gb claim\nprocessor" as agent
'box "AFPC" #CCD5EB
box "Apply for Pension Credit"
participant "Agent\nWorkflow UI" as workflow 
participant "Assessment\nService" as assess 
participant "Claim\nService" as claim 
participant "Award\nService" as award 
end box
participant "DWP\nTask" as task #99FF99
participant CIS
participant "DWP\nNotifications" as notify #99FF99

== process a claim ==
agent -> workflow: process a claim
activate workflow #CC4125
workflow -> task: check agent exists (create/update if missing)
activate task #CC4125
workflow <-- task 
deactivate task
workflow -> task: check if agent already has task assigned
activate task #CC4125
workflow <-- task
deactivate task
workflow -> task: claim and activate next agent task
activate task #CC4125
workflow <-- task: <<assessment id>>
deactivate task
workflow -> agent: 302 redirect to assessment url
deactivate workflow 

agent -> workflow: show assessment
activate workflow #CC4125
workflow -> assess: get assessment by id
activate assess #CC4125
workflow <-- assess: <<assessment details, claim id>>
deactivate assess
workflow -> claim: get claim by id
activate claim #CC4125
workflow <-- claim: <<claim details>>
deactivate claim
agent <-- workflow: render page
deactivate workflow 

== perform sub tasks == 
loop
  agent -> workflow: show sub task
  activate workflow #CC4125
  workflow -> assess: get sub task
  activate assess #CC4125
  workflow <-- assess
  deactivate assess
  
  workflow -> claim: get claim attribute (optional)
  activate claim #CC4125
  workflow <-- claim
  deactivate claim
  
  agent <-- workflow: render page
  deactivate workflow
  agent -> workflow: update sub task result
  activate workflow #CC4125
  workflow -> assess: update sub task
  activate assess #CC4125
  workflow <-- assess
  deactivate assess
  agent <-- workflow: render pages
  deactivate workflow
end

== complete task ==
agent -> workflow: display list of tasks
activate workflow #CC4125
workflow -> assess: get assessment by id
activate assess #CC4125
assess --> assess: validate sub tasks
workflow <-- assess: <<subTaskStatus=DECISION_CONFIRMED>> 
deactivate assess
agent <-- workflow: render "Confirm and notify" button
deactivate workflow
agent -> workflow: submit "Confirm and notify"
activate workflow #CC4125
workflow -> assess: Confirm and notify
activate assess #CC4125
assess --> assess: set Decision = NOT_ENTITLED
assess -> CIS: create open interest (SQS)
assess -> notify: send letter request (SQS)
assess -> task: mark task complete
activate task #cc0000
assess<--task
deactivate task
workflow <-- assess
deactivate assess

agent <-- workflow: render home page?
deactivate workflow


```

